%define srcname ctypesgen
%define version 1.0.2
%define release 1

Name:    python-%{srcname}
Version: %{version}
Release: %{release}%{?dist}
Summary: Python wrapper generator for ctypes
License: BSD-2-Clause
URL:     https://github.com/ctypesgen/ctypesgen
#Source0: %%pypi_source %%srcname
Source0: https://github.com/ctypesgen/ctypesgen/archive/%{srcname}-%{version}.tar.gz
Packager:  Duncan Macleod <duncan.macleod@ligo.org>

BuildArch: noarch

# build requirements
BuildRequires: python%{python3_pkgversion}-devel >= 3.6
BuildRequires: python%{python3_pkgversion}-pip
BuildRequires: python%{python3_pkgversion}-setuptools
BuildRequires: python%{python3_pkgversion}-wheel

# -- src.rpm

%description
ctypesgen reads parses c header files and creates a wrapper for
libraries based on what it finds.  Preprocessor macros are handled
in a manner consistent with typical c code.  Preprocessor macro
functions are translated into Python functions that are then made
available to the user of the newly-generated Python wrapper library.
ctypesgen can also output JSON, which can be used with Mork,
which generates bindings for Lua, using the alien module (which
binds libffi to Lua).

# -- python3-ctypesgen

%package -n python%{python3_pkgversion}-%{srcname}
Summary: Python wrapper generator for ctypes (Python %{python3_version} library)
%{?python_provide:%python_provide python%{python3_pkgversion}-%{srcname}}
%description -n python%{python3_pkgversion}-%{srcname}
ctypesgen reads parses c header files and creates a wrapper for
libraries based on what it finds.
This package provides the Python %{python3_version} library.

# -- ctypesgen

%package -n %{srcname}
Summary: Python wrapper generator for ctypes (console scripts)
Requires: python%{python3_pkgversion}-%{srcname} = %{version}-%{release}
%description -n %{srcname}
ctypesgen reads parses c header files and creates a wrapper for
libraries based on what it finds.
This package provides the Python %{python3_version} library.

# -- build

%prep
%setup -q -n %{srcname}-%{srcname}-%{version}

%build
# force correct version into version file
echo "%{srcname}-%{version}" > ctypesgen/VERSION
# build a wheel
%py3_build_wheel

%install
%py3_install_wheel %{srcname}-%{version}-*.whl

# -- files

%files -n %{srcname}
%license LICENSE
%doc README.md
%{_bindir}/*

%files -n python%{python3_pkgversion}-%{srcname}
%license LICENSE
%doc README.md
%{python3_sitelib}/*

# -- changelog

%changelog
* Tue Jan 9 2024 Duncan Macleod <duncan.macleod@ligo.org> 1.0.2-1
- first build for IGWN RHEL 7/8/9
